# AirTable/Youtube Automation tool

Application that automatically detects changes in Airtable database, and updates YouTube videos.

### How it works:
1. Application detects changes in Airtable database (record's status changed to "Done")
2. It opens YouTube, searches for the updated video, and updates the changes according to the one present in the database, at the time of the status change.

### Technologies:
* Node.js,
* Airtable.js,
* Dotenv,
* Playwright,
* TypeScript,
* TS-Node,

### How to run:

### How to run:
If you're running it for the first time:
```
> npm i
> npm --max-old-space-size=4096 start
```

And later you can just use:

```
> npm --max-old-space-size=4096 start
```