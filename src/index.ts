import 'dotenv/config';
import { Page, firefox, Browser, BrowserContext } from 'playwright';
import login from './login';
import { Page, firefox, Browser, BrowserContext } from 'playwright';
import login from './login';
import AirTable from 'airtable';

const STUDIO_CHANEL_URL =
'https://studio.youtube.com/channel/UCBviUkaeHb3SQPdtIRTNtJQ/videos/upload'  // OFISZIAL
    // "https://studio.youtube.com/channel/UCeIIBVKrJMRKyRT2WpJcEmA/videos/upload"    
    // 'https://studio.youtube.com/channel/UCLHMs6GoCv7YgsF3ZMxnf8A/videos/upload'; // Tutaj jest URL tego kanału TESTY
const LOGIN = process.env["YT_EMAIL"]; // adres email do konta Google
const PASSWORD = process.env["YT_PASSWORD"]; // hasło do konta Google - można zostawić puste - będzie możliwość wpisania przy logowaniu

let browser: Browser;
let context: BrowserContext;
let page: Page;

// Logs how much memory is used (in MB)
function logMemoryUsed() {
    console.log(`Used: ${Math.floor(process.memoryUsage().heapUsed / 1024 / 1024)}`);
}

const STUDIO_CHANEL_URL =
'https://studio.youtube.com/channel/UCBviUkaeHb3SQPdtIRTNtJQ/videos/upload'  // OFISZIAL
    // "https://studio.youtube.com/channel/UCeIIBVKrJMRKyRT2WpJcEmA/videos/upload"    
    // 'https://studio.youtube.com/channel/UCLHMs6GoCv7YgsF3ZMxnf8A/videos/upload'; // Tutaj jest URL tego kanału TESTY
const LOGIN = process.env["YT_EMAIL"]; // adres email do konta Google
const PASSWORD = process.env["YT_PASSWORD"]; // hasło do konta Google - można zostawić puste - będzie możliwość wpisania przy logowaniu

let browser: Browser;
let context: BrowserContext;
let page: Page;

// Logs how much memory is used (in MB)
function logMemoryUsed() {
    console.log(`Used: ${Math.floor(process.memoryUsage().heapUsed / 1024 / 1024)}`);
}

export class AirTableWatcher {

    private baseInstance: AirTable.Base;

    constructor(
        private requestTime: number,
        private apiKey: string | undefined,
        private base: string,
        private table: string,
    ) {
        AirTable.configure({ apiKey: this.apiKey });
        this.baseInstance = AirTable.base(this.base);
        this.setup().then(async () => {
            await this.watch();
        })
    }

    private async setup() {
        browser = await firefox.launch({ headless: false });
        context = await browser.newContext({ storageState: "state.json" });
        page = await context.newPage();

        await context.close();
        await page.close();
        this.setup().then(async () => {
            await this.watch();
        })
    }

    private async setup() {
        browser = await firefox.launch({ headless: false });
        context = await browser.newContext({ storageState: "state.json" });
        page = await context.newPage();

        await context.close();
        await page.close();
    }

    private moveRecord(record: AirTable.Record<AirTable.FieldSet>, id: string | undefined) {
        this.baseInstance("EditedVideos").create([{
            fields: {
                Filename: record.get("Filename"),
                Assignee: record.get("Assignee"),
                Title: record.get("Title"),
                Description: record.get("Description"),
            }
        }]);

        // Checks if the id is undefined and stops the execution of the function if that is the case
    private moveRecord(record: AirTable.Record<AirTable.FieldSet>, id: string | undefined) {
        this.baseInstance("EditedVideos").create([{
            fields: {
                Filename: record.get("Filename"),
                Assignee: record.get("Assignee"),
                Title: record.get("Title"),
                Description: record.get("Description"),
            }
        }]);

        // Checks if the id is undefined and stops the execution of the function if that is the case
        if (id === undefined) {
            return;
        }

        this.baseInstance(this.table).destroy(id, (err, deletedRecord) => {
            if (err) {
                console.error(err);
                return;
            }

            const date = new Date();

            console.log(`Przeniesiono do tabeli EditedVideos ${id} | Data: ${date.toLocaleString("us-US")}`);
            const date = new Date();

            console.log(`Przeniesiono do tabeli EditedVideos ${id} | Data: ${date.toLocaleString("us-US")}`);
        })
    }

    private async watch(): Promise<void> {
    private async watch(): Promise<void> {
        this.baseInstance(this.table).select({
            maxRecords: 500,
            filterByFormula: `OR({Status} = 'Done', {Status} = 'Public')`,
            view: "Grid view"
        }).eachPage(async (records, next) => {
            for (const record of records) {
                    try {
                        let youtubeid = (record.get("URL") as string).split('/').at(-1);

                        if (record.get('Status') === 'Done') {

                            if (record.get("Title") === undefined || record.get("Description") === undefined) {
                                console.log("Pusty opis/tytuł, pomijamy.");
                                continue;
                            }
                            
                            if (youtubeid !== undefined) {
                                youtubeid = youtubeid.trim();
                            }

                            context = await browser.newContext({ storageState: "state.json" });
                            page = await context.newPage();
                            await page.goto(`https://studio.youtube.com/video/${youtubeid}/edit`);
                            
                            await page.waitForTimeout(10000);

                            if (await ((await page.$('#textbox'))?.getAttribute("contenteditable")) !== "false") {   
                                (await page.$$('#textbox'))[0].fill(record.get("Title") as string);
                                (await page.$$('#textbox'))[1].fill(record.get("Description") as string);
                            } else {
                                throw new Error("error1");
                            }

                            await page.waitForTimeout(5000);
                            await page.waitForSelector("ytcp-button#save:not([disabled])");
                            await page.click('ytcp-button#save');
                            this.moveRecord(record, record.id as string | undefined);
                        } else if (record.get('Status') === 'Public') {

                            if (record.get("Title") === undefined || record.get("Description") === undefined) {
                                console.log("Pusty opis/tytuł, pomijamy.");
                                continue;
                            }

                            if (youtubeid !== undefined) {
                                youtubeid = youtubeid.trim();
                            }

                            context = await browser.newContext({ storageState: "state.json" });
                            page = await context.newPage();
                            await page.goto(`https://studio.youtube.com/video/${youtubeid}/edit`);

                            await page.waitForTimeout(10000);

                            if (await ((await page.$('#textbox'))?.getAttribute("contenteditable")) !== "false") {   
                                (await page.$$('#textbox'))[0].fill(record.get("Title") as string);
                                (await page.$$('#textbox'))[1].fill(record.get("Description") as string);
                            } else {
                                throw new Error("error1");
                            }

                            await page.click('.ytcp-video-metadata-visibility');
                            await page.waitForSelector('tp-yt-paper-radio-button[name="PUBLIC"]');
                            await page.click('tp-yt-paper-radio-button[name="PUBLIC"]');
                            await page.waitForSelector('#save-button');
                            await page.click('#save-button');
                            await page.waitForTimeout(5000);
                            await page.waitForSelector("ytcp-button#save:not([disabled])");
                            await page.click('ytcp-button#save');
                            this.moveRecord(record, record.id as string | undefined);
                        }

                    } catch(e) {
                        console.log(`Błąd: ${e}`);
                        console.log( records );
                        continue;
                    } finally {   
                        await context.close();
                        await page.close();         
                    }
            }
            console.log("next")
            next();
        }, async (err) => {
            console.log("next")
            next();
        }, async (err) => {
            if (err) { console.error(err) };
            setTimeout(async () => await this.watch(), this.requestTime);
            setTimeout(async () => await this.watch(), this.requestTime);
        });
    }

}

new AirTableWatcher(
    6000,
    6000,
    process.env["AIRTABLE_KEY"],
    "appZ6xrU3emUyZ3KE",
    "Videos",
);